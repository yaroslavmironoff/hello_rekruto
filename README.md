# hello_rekruto


Создать веб-сервис, который будет выводить "Rekruto! Давай дружить!". 
URL с GET запросом, который будет выводиться уже на странице: урл должен быть таким: 
"url_name/?name=Rekruto&message=Давай дружить!"
 и выводиться на странице Hello {name}! {message}!

Ответ на тестовое: ссылка на веб-сервис.

http://yaroslavmironoff.pythonanywhere.com/Rekruto/Давай%20дружить!
